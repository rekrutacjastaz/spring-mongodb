package com.example.nosqldemo.repository;

import org.bson.types.ObjectId;
import org.springframework.data.repository.CrudRepository;

import com.example.nosqldemo.domain.Manufacturer;

public interface ManufacturerRepository extends
		CrudRepository<Manufacturer, ObjectId> {

	Manufacturer findById(ObjectId id);

	Manufacturer findByName(String name);
}
