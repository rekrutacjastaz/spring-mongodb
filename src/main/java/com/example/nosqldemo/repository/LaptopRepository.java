package com.example.nosqldemo.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.nosqldemo.domain.Laptop;
import com.example.nosqldemo.domain.Manufacturer;

public interface LaptopRepository extends CrudRepository<Laptop, ObjectId> {

	List<Laptop> findByModel(String model);

	List<Laptop> findByUltrabook(boolean isUltrabook);

	Laptop findById(ObjectId id);

	List<Laptop> findByManufacturerName(String manufacturerName);

	List<Laptop> findByManufacturer(Manufacturer manufacturer);

	@Query(value = "{ model : ?0, manufacturer.name : ?1 }")
	List<Laptop> findLaptopsByModelAndManufacturersName(String model,
			String manufacturersName);

	@Query(value = "{ model : ?0, manufacturer.name : ?1 }")
	void deleteLaptopsByModelAndManufacturersName(String model,
			String manufacturersName);
}
